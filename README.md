# BankConnect API wrapper

## USAGE:

subclass APIClient class and provide Your custom api certtificate and private rsa key as .pem extension
also account number is required, most of methods and functions are self explanatory

## EXAMPLE:

    from bconnect import APIClient

    class MyClient(APIClient):
        api_cert = 'path/to/cert/file'
        api_key = 'path/to/key/file'
        account_number = '<26 digits BBAN>'


    client = MyClient()

or:

    client = APIClient(api_cert='path/to/cert/file', api_key='path/to/key/file',
                       account_number='<26 digits BBAN>')



### USEFUL METHODS:

account history object:

    history = client.get_history(<start_date>, <end_date>)
    # date in iso format
    # returns History object

Handful `History()` methods

full report:

    history.full_report

payments:

    history.payments

charges:

    history.charges

account balance:

    history.balance


