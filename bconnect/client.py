#-*- coding: utf-8 -*-

from suds.client import Client
from handlers import HTTPSClientCertTransport
import random
from collections import namedtuple
from .exceptions import BankError, HttpResponseError

Transfer = namedtuple('Transfer', ['id',
                                   'number',
                                   'type',
                                   'amount',
                                   'currency',
                                   'posting_date',
                                   'operation_date',
                                   'account_number',
                                   'account_holder_name',
                                   'account_holder_address',
                                   'account_holder_country',
                                   'transaction_name'])


class History(object):

    def __init__(self, history, *args, **kwargs):
        super(History, self).__init__(*args, **kwargs)
        self.report = history.Rpt[0]

    @property
    def full_report(self):
        Report = namedtuple('Report', ['id', 'creation_datetime', 'period', 'account', 'balance', 'transactions'])
        full_report = {
            'id': self.report.Id.encode('utf-8'),
            # TZ aware datetime in iso format
            'creation_datetime': self.report.CreDtTm.encode('utf-8'),
            # (start, end) date tuple
            'period': (self.report.FrToDt.FrDtTm.encode('utf-8'), self.report.FrToDt.ToDtTm.encode('utf-8')),
            'account': {
                'bban': self.report.Acct.Id.BBAN.encode('utf-8'),
                'holder_name': self.report.Acct.Ownr.Nm.encode('utf-8'),
                'holder_address': [addr.encode('utf-8') for addr in self.report.Acct.Ownr.PstlAdr.AdrLine],
                'holder_country': self.report.Acct.Ownr.PstlAdr.Ctry.encode('utf-8'),
            },
            'balance': self.balance,
            'transactions': {
                'payments': self.payments,
                'charges': self.charges
            }
        }
        return Report(**full_report)

    @property
    def transfers(self):
        transfers = {'payments': [], 'charges': []}
        _transfer_types = {'DBIT': 'charges', 'CRDT': 'payments'}
        entries = getattr(self.report, 'Ntry', [])
        for entry in entries:
            transfer = {}
            typ = getattr(entry, 'CdtDbtInd', '').encode('utf-8')
            transfer['type'] = typ
            transfer['amount'] = entry.Amt.value.encode('utf-8')
            transfer['currency'] = entry.Amt._Ccy.encode('utf-8')
            transfer['posting_date'] = entry.BookgDt.DtTm.encode('utf-8')
            transfer['operation_date'] = entry.ValDt.DtTm.encode('utf-8')
            transaction = entry.TxDtls[0]
            transaction_party = getattr(transaction, 'RltdPties', None)
            transfer['id'] = transaction.Refs.InstrId.encode('utf-8')
            transfer['number'] = transaction.Refs.TxId.encode('utf-8')
            if transaction_party:
                cash_account = getattr(transaction_party, 'DbtrAcct',
                                       getattr(transaction_party, 'CdtrAcct', None))
                party_info = getattr(transaction_party, 'Dbtr',
                                     getattr(transaction_party, 'Cdtr', None))
                transfer['account_number'] = getattr(cash_account.Id, 'BBAN',
                                                     getattr(cash_account.Id, 'IBAN', '')).encode('utf-8') if cash_account else ''
                transfer['account_holder_name'] = party_info.Nm.encode('utf-8') if party_info else ''
                address_line = getattr(getattr(party_info, 'PstlAdr', None), 'AdrLine', None)
                transfer['account_holder_address'] = address_line[0].encode('utf-8') if address_line else ''
                transfer['account_holder_country'] = getattr(getattr(party_info, 'PstlAdr', None),
                                                             'Ctry', '').encode('utf-8')
            else:
                transfer['account_number'] = ''
                transfer['account_holder_name'] = ''
                transfer['account_holder_address'] = ''
                transfer['account_holder_country'] = ''
            transfer['transaction_name'] = getattr(transaction.RmtInf,'Ustrd',
                                                    getattr(transaction.RmtInf,'Strd',
                                                            ['n.d.']))[0].encode('utf-8')
            transfers[_transfer_types[typ]].append(Transfer(**transfer))
        return transfers

    @property
    def balance(self):
        balance_list = self.report.Bal
        balance = {}
        for bal in balance_list:
            if bal.Tp.Cd == 'OPBD':
                balance['opening'] = {
                    'amount': bal.Amt.value.encode('utf-8'),
                    'currency': bal.Amt._Ccy.encode('utf-8'),
                    'type': bal.CdtDbtInd.encode('utf-8'),
                    'date': bal.Dt.Dt.encode('utf-8'),
                }
            elif bal.Tp.Cd == 'CLBD':
                balance['closing'] = {
                    'amount': bal.Amt.value.encode('utf-8'),
                    'currency': bal.Amt._Ccy.encode('utf-8'),
                    'type': bal.CdtDbtInd.encode('utf-8'),
                    'date': bal.Dt.Dt.encode('utf-8'),
                }
        return balance

    @property
    def payments(self):
        return self.transfers['payments']

    @property
    def charges(self):
        return self.transfers['charges']


class APIResult(namedtuple('APIResult', ['status_code',  'content'])):

    @property
    def result(self):
        if self.status_code != 200:
            raise HttpResponseError(self.status_code, self.content)
        else:
            return self.content

    def get_document_or_raise_api_error(self):
        # BankConnect API returns response as special type documents
        document_error = getattr(self.result, 'DocumentError', None)
        if not document_error:
            document = getattr(self.result, 'Document', None)
            if document:
                return document
            else:
                raise ValueError('Document not found in BankConnect Response')
        else:
            error_code = document_error.OprlErr.ErrCd
            description = document_error.OprlErr.Desc
            raise BankError(error_code, description)

    def history(self):
        document = self.get_document_or_raise_api_error()
        return History(history=document.BkToCstmrAcctRptV01)


class APIClient(object):
    """
    API client class, subclass this with Your own client class and provide api_cert, api_key
    and account_number or construct it providing those arguments as keyword arguments.
    """
    wsdl_file = 'https://bankconnect.aliorbank.pl/bankconnect/bank-connect/service.wsdl'
    api_cert = None
    api_key = None
    account_number = None

    def __init__(self, timeout=3, *args, **kwargs):
        self.api_cert = kwargs.pop('api_cert', self.api_cert)
        self.api_key = kwargs.pop('api_key', self.api_key)
        self.account_number = kwargs.pop('account_number', self.account_number)
        super(APIClient, self).__init__(*args, **kwargs)
        assert self.api_cert, ('You need to subclass APIClient and provide API certificate')
        assert self.api_key, ('You need to subclass APIClient and provide API key')
        assert self.account_number, ('You need to subclass APIClient and provide account number')
        # create client with 3s timeout and (<status>, <returned_value>) response structure,
        self.client = Client(self.wsdl_file, transport=HTTPSClientCertTransport(self.api_key, self.api_cert),
                             faults=False, timeout=timeout)


    def get_msg_id(self):
        prefix = 'REQ_'
        sample = ''.join(random.sample('1234567890QWERTYUIOPASDFGHJKLZXCVBNM', 16))
        return '%s%s' % (prefix, sample)

    def get_operation_history_document(self, start_date, end_date):
        """
        Method creates document for account operation history,
        it takes start and end date in iso format
        """
        document = self.client.factory.create('ns1:Document')
        document.GetAcctRpt.MsgId.Id = self.get_msg_id()
        setattr(document.GetAcctRpt.AcctRptQryDef.AcctRptCrit.NewCrit.SchCrit.AcctId.EQ, 'BBAN', self.account_number)
        document.GetAcctRpt.AcctRptQryDef.AcctRptCrit.NewCrit.SchCrit.AcctRptValDt.DtSch.FrDt = start_date
        document.GetAcctRpt.AcctRptQryDef.AcctRptCrit.NewCrit.SchCrit.AcctRptValDt.DtSch.ToDt = end_date
        return document

    def get_history(self, start_date, end_date):
        history = getattr(self.client.service, 'GetOperationsHistory')
        document = self.get_operation_history_document(start_date, end_date)
        result_code, result_content = history(document)
        return APIResult(status_code=result_code, content=result_content).history()
