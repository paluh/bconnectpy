#!/usr/bin/env python
#-*- coding: utf-8 -*-
import unittest
import os
from ..exceptions import BankError

from ..client import APIClient


class Client(APIClient):

    api_cert = os.path.join(os.path.dirname(__file__), 'test_cert.pem')
    api_key = os.path.join(os.path.dirname(__file__), 'test_key.pem')
    account_number = '22106000760000321000137627'


class TestClient(unittest.TestCase):

    def setUp(self):
        self.client = APIClient(api_cert= os.path.join(os.path.dirname(__file__), 'test_cert.pem'),
                                api_key=os.path.join(os.path.dirname(__file__), 'test_key.pem'),
                                account_number='22106000760000321000137627')

        self.subclassed_client = Client()

    def test_get_report_with_invalid_bban_fails(self):
        self.client.account_number = '123'
        with self.assertRaises(BankError):
            self.client.get_history('2012-10-03', '2012-10-08')

    def test_get_report_with_invalid_date_format_fails(self):
        with self.assertRaises(BankError):
            self.client.get_history('2012-10-3', '2012-10-8')

    def test_payments(self):
        h = self.client.get_history('2012-10-03', '2012-10-08')
        print h.payments

    def test_full_report(self):
        h = self.client.get_history('2012-10-03', '2012-10-08')
        print h.full_report

    # TODO: more complex and reliable tests with MockServices

if __name__ == '__main__':
    unittest.main()